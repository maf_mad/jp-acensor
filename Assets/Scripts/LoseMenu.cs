﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoseMenu : MonoBehaviour
{

    public Sprite pressPlayButton;
    public Sprite pressExitButton;
    public void TryAgain()
    {
        FindObjectOfType<AudioManager>().Play("Boton2");
        Manager.points = 0;
        GameObject.Find("Button").GetComponent<Image>().sprite = pressPlayButton;
        SceneManager.LoadScene("Game");
    }
    public void QuitGame()
    {
        FindObjectOfType<AudioManager>().Play("Boton");
        GameObject.Find("Button (1)").GetComponent<Image>().sprite = pressExitButton;
        Application.Quit();
    }

}