﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ascensor : MonoBehaviour
{
    public int currentPeopleOn = 0;
    private int maxPeople = 5;
    public bool canEnter;
    public bool isGrounded;

    public float speed;
    public Transform floor1;
    public Transform floor2;
    public Transform floor3;
    public Transform floor4;
    public Transform floor5;
    public Transform floor6;
    public Transform floor7;
    public Transform floor8;
    public Transform floor9;
    public Transform floor10;

    public int target;

    public int currentFloor;

    public List<Transform> waitPoints;
    public bool point1taken;
    public bool point2taken;
    public bool point3taken;
    public bool point4taken;
    public Transform nextPoint; 

    void Start()
    {
        canEnter = true;
        isGrounded = true;
    }

    void Update()
    {
        if(currentPeopleOn == maxPeople)
        {
            Debug.Log("lleno");
            canEnter = false;
        }
        else
        {
            canEnter = true;
        }

        if (target == 1)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor1.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor1.position) <= 0)
            {
                isGrounded = true;
                currentFloor = 1;
            }
        }
        if (target == 2)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor2.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor2.position) <= 0)
            {
                currentFloor = 2;
                
            }
        }
        if (target == 3)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor3.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor3.position) <= 0)
            {
                currentFloor = 3;
                
            }
        }
        if (target == 4)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor4.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor4.position) <= 0)
            {
                currentFloor = 4;
            
            }
        }
        if (target == 5)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor5.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor5.position) <= 0)
            {
                currentFloor = 5;
               
            }
        }
        if (target == 6)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor6.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor6.position) <= 0)
            {
                currentFloor = 6;
              
            }
        }
        if (target == 7)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor7.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor7.position) <= 0)
            {
                currentFloor = 7;
             
            }
        }
        if (target == 8)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor8.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor8.position) <= 0)
            {
                currentFloor = 8;
            
            }
        }
        if (target == 9)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor9.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor9.position) <= 0)
            {
                currentFloor = 9;
        
            }
        }
        if (target == 10)
        {
            transform.position = Vector2.MoveTowards(transform.position, floor10.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, floor10.position) <= 0)
            {
                currentFloor = 10;
               

            }
        }
        CheckPeopleOnElevator();

        if (currentPeopleOn <= 0)
        {
            nextPoint = waitPoints[0];
            currentPeopleOn = 0;
        }
        if (currentPeopleOn >= 1)
        {
            point1taken = true;
        }
        else
        {
            point1taken = false;
        }
        if (currentPeopleOn >= 2)
        {
            point2taken = true;
        }
        else
        {
            point2taken = false;
        }
        if (currentPeopleOn >= 3)
        {
            point3taken = true;
        }
        else
        {
            point3taken = false;
        }
        if (currentPeopleOn >= 4)
        {
            point4taken = true;
        }
        else
        {
            point4taken = false;
        }
    }

    public void MoveToFloor1()
    {
        target = 1;
    }
    public void MoveToFloor2()
    {
        target = 2;
        isGrounded = false;
    }
    public void MoveToFloor3()
    {
        target = 3;
        isGrounded = false;
    }
    public void MoveToFloor4()
    {
        target = 4;
        isGrounded = false;
    }
    public void MoveToFloor5()
    {
        target = 5;
        isGrounded = false;
    }
    public void MoveToFloor6()
    {
        target = 6;
        isGrounded = false;
    }
    public void MoveToFloor7()
    {
        target = 7;
        isGrounded = false;
    }
    public void MoveToFloor8()
    {
        target = 8;
        isGrounded = false;
    }
    public void MoveToFloor9()
    {
        target = 9;
        isGrounded = false;
    }
    public void MoveToFloor10()
    {
        target = 10;
        isGrounded = false;
    }

    public void CheckPeopleOnElevator()
    {
        if (point1taken == true)
        {
            nextPoint = waitPoints[1];
        }
        if (point2taken == true)
        {
            nextPoint = waitPoints[2];
        }
        if (point3taken == true)
        {
            nextPoint = waitPoints[3];
        }
        if (point4taken == true)
        {
            nextPoint = waitPoints[4];
        }
    }
}
