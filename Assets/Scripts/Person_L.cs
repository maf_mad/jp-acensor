﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Person_L : MonoBehaviour
{
    public Transform firstPoint;
    public Transform ascensorPosition;
    public Transform ascensorPersonPosition;
    public float speed = 2f;
    bool reachedFirstPoint = false;

    Ascensor ascensorComp;
    Manager managerComp;
    Lobby lobbyComp;

    public int targetFloor;
    public bool isWaiting;
    public bool isOnElevator = false;

    public Text playerText;
    Anim animComp;

    void Start()
    {
        ascensorPosition = GameObject.Find("Ascensor1").GetComponent<Transform>();
        ascensorComp = GameObject.Find("Ascensor1").GetComponent<Ascensor>();
        managerComp = GameObject.Find("GameManager").GetComponent<Manager>();
        lobbyComp = GameObject.Find("QueueLobby_L").GetComponent<Lobby>();
        animComp = GetComponent<Anim>();

        firstPoint = lobbyComp.nextPoint;
        ascensorPersonPosition = ascensorComp.nextPoint;

        isWaiting = false;
        targetFloor = Random.Range(2, 11);
        SetPlayerText();

        animComp.WalkAnim();
    }

    void Update()
    {
        if (reachedFirstPoint == false)
        {
            MoveToFirstPoint();
        }
        if (ascensorComp.canEnter == true && ascensorComp.isGrounded == true && isWaiting == true)
        {
            managerComp.peopleWaiting--;
            Entrar();
            Debug.Log("CurrentPeopleWaiting: " + managerComp.peopleWaiting);
        }
        if (isOnElevator == true)
        {
            gameObject.transform.SetParent(ascensorPosition);
            if (targetFloor == ascensorComp.currentFloor)
            {
                ascensorComp.currentPeopleOn--;
                managerComp.GetPoints();
                managerComp.SetPointsText();
                Destroy(gameObject);
            }
        }
    }

    void MoveToFirstPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, firstPoint.position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, firstPoint.position) <= 0)
        {
            reachedFirstPoint = true;
            animComp.IdleAnim();
            Debug.Log("Llegó");
            if (ascensorComp.canEnter == true && ascensorComp.isGrounded == true)
            {
                Entrar();
            }
            else
            {
                Esperar();
            }
        }
    }
    void Entrar()
    {
        transform.position = ascensorPersonPosition.position;
        ascensorComp.currentPeopleOn++;
        lobbyComp.currentPeopleOnQueue--;
        isWaiting = false;
        isOnElevator = true;
    }
    void Esperar()
    {
        isWaiting = true;
        lobbyComp.currentPeopleOnQueue++;
        managerComp.peopleWaiting++;
        Debug.Log("CurrentPeopleWaiting: " + managerComp.peopleWaiting);
    }
    void SetPlayerText()
    {
        playerText.text = targetFloor.ToString();
    }
}